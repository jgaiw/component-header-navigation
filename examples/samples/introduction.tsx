/* tslint:disable:max-line-length */
import { Button } from 'component-button';
import * as styles from 'designs/css/application.min.css';
import * as React from 'react';
import Header from '../../header';

const doAlert = () =>  alert('Clicked a Button');
const onSubmit = (value: string) => alert(value);
// tslint:disable-next-line:no-console
const valueDidChange = (value: string) => console.log(value);
const buttons: JSX.Element = (
    <div>
        <div className={`${styles.dib}`}>
            <Button onClick={doAlert} style="navigation">Sign In</Button>
        </div>
        <div className={`${styles.dib} ${styles.pl2}`}>
            <Button onClick={doAlert} style="navigation">Redeem</Button>
        </div>
        <div className={`${styles.dib} ${styles.pl2}`}>
            <Button onClick={doAlert}> Buy</Button>
        </div>
    </div>
);

const icon = (
    <a href="#" title="tastecard homepage" className={`${styles.iwCenter}`}>
        <svg xmlns="http://www.w3.org/2000/svg" width="100" height="30" viewBox="0 0 327 66"><g fill="none"><path fill="#82BC00" d="M22.08 17.93H11.99V3.52H8.57v14.41H0v3h8.56v32.35c-.36 10.46 6.4 11.44 13.43 11v-3c-6.49.18-10-.54-10-8V20.91h10.09v-2.98zm34.31 28.66c0 2.43-1.71 15.77-17.3 15.77-8.11 0-12.17-4.51-12.17-10.09 0-6.94 4.6-9.83 12.71-10.73 6.67-.9 14.06-.54 16.76-3.61v8.66zm3.42-17.66c0-9.55-8.11-12.08-16.22-12.08-10.91 0-17.22 5.14-17.67 15.28h3.42c.45-8 5.95-12.35 14.24-12.35 10.54 0 12.89 7 12.89 9.1 0 7.57-.63 8.56-16 9.64-7.66.63-17 2.88-17 13.7 0 9.73 6.76 13.07 15.5 13.07s15.05-4.77 17.21-10.27h.18v2.43c0 3.78.81 6.76 6.22 6.76a15.86 15.86 0 0 0 2.79-.36v-2.88a16 16 0 0 1-2.52.27c-3.06 0-3.06-2.88-3.06-5.59l.02-26.72zm6.4 19.91c.27 10.82 8.2 16.49 18.75 16.49 8.11 0 18.11-2.79 18.11-12.8 0-9.73-7.21-11.9-16.4-13.61-7.48-1.62-15.41-2.79-15.41-10.18 0-6.58 7-8.92 13.34-8.92 7.75 0 13.61 3.42 13.7 11.81h3.42c0-10.54-7.3-14.78-17.12-14.78-7.84 0-16.76 2.7-16.76 11.9 0 9.46 7.93 11.53 16 13.16 8.07 1.63 15.86 2.79 15.86 10.64 0 8-8.65 9.82-14.69 9.82-8.38 0-14.78-5-15.32-13.52l-3.48-.01zm59.47-30.91h-10.09V3.52h-3.42v14.41h-8.56v3h8.56v32.35c-.36 10.46 6.4 11.44 13.43 11v-3c-6.49.18-10-.54-10-8V20.91h10.08v-2.98zM168 41.55c.63-13.25-5.86-24.69-20.37-24.69-14 0-20.64 12.35-20.46 25.05-.18 12.8 6.94 23.43 20.46 23.43 11 0 18.2-5.95 20-16.76h-3.42c-1.62 8.56-7.84 13.79-16.58 13.79-11.45 0-17.21-9.91-17-20.82H168zm-37.4-3c.72-9.19 6.76-18.74 17-18.74 10.64 0 16.76 9 16.94 18.74H130.6z"/>
        <path fill="#FFF" d="M212.48 32.45c-1.26-10.64-8.92-16-18.84-16-14.24 0-22 11.27-22 24.61s7.75 24.61 22 24.61c10.37 0 17.67-6.85 19.2-18h-5.68c-.72 7.75-6.49 13.25-13.52 13.25-10.91 0-16.31-9.92-16.31-19.83 0-9.91 5.41-19.83 16.31-19.83 7.39 0 11.63 4.24 13.16 11.18l5.68.01zm35.31 13.88c.09 9.1-7.57 14.6-16.23 14.6-5.32 0-10.37-3.51-10.37-9.19 0-9.1 12-9 21.63-10.73 1.53-.27 4.06-.72 4.78-2.07h.18l.01 7.39zm-24.61-14.2c.18-7.57 5.41-10.82 12.44-10.82 6.67 0 12.17 1.89 12.17 9.55 0 5.14-2.61 5.77-7.3 6.31-12.26 1.44-25 1.89-25 15 0 9.37 7 13.61 15.59 13.61 8.92 0 13-3.43 16.95-9.46h.18c0 4.87.9 8.11 6.67 8.11a15.63 15.63 0 0 0 3.69-.36v-4.78a5.31 5.31 0 0 1-1.8.36c-2.25 0-3.34-1.26-3.34-3.43V31.46c0-12.44-8.92-15-17.13-15-10.6.03-18.26 4.67-18.82 15.67h5.7zm38.49 32.27h5.68V39.57c0-9.64 7-17.22 17.31-16.67v-5.69c-8.38-.36-14.69 4.06-17.58 11.54h-.18V17.84h-5.23V64.4zm27.76-23.27c0-9.64 4.42-19.83 15.23-19.83 12.08 0 16.31 10.19 16.31 19.83 0 9.64-4.24 19.83-16.31 19.83-10.82 0-15.23-10.19-15.23-19.83zM326.66.03h-5.68v26.65h-.18c-2.43-6.67-9.2-10.19-16.13-10.19-14.19 0-20.92 11.45-20.92 24.64 0 13.19 6.76 24.61 20.91 24.61 6.76 0 14.15-4.15 16.58-10.19h.24v8.83h5.23L326.66.03z"/></g></svg>
    </a>
);

const navContent = (
    <nav className={`
        ${styles.absolute}
        ${styles.dnNs}
        ${styles.bgDarkGrey}
        ${styles.white}
        ${styles.left0}
        ${styles.w100}
        ${styles.transition05}
        ${styles.vh100}
        ${styles.ptFixed64}
        ${styles.overflowYAuto}
        `}>
        <div className={`${styles.mh3} ${styles.pv3} ${styles.bMidGrey} ${styles.bb} ${styles.bwFixed1}`}>
            <p className={`${styles.ma0}`}>
            <a href="#" className={`${styles.f6}`}>
            Join <span className={`${styles.green}`}>2,574,268</span>
            Members who get <span className={`${styles.green}`}>2 for 1</span>
            meals at over <span className={`${styles.green}`}>6,000</span>
            restaurants</a></p>
        </div>

        <div className={`
            ${styles.flex}
            ${styles.mh3}
            ${styles.pv3}
            ${styles.ph0}
            ${styles.mv0}
            ${styles.bMidGrey}
            ${styles.bb} ${styles.bwFixed1}
        `}>
            <div className={`${styles.flex} ${styles.iwFlex1}`}>
                <div className={`${styles.iwCenter}`}>
                    <a href="#">
                        <div className={`
                            ${styles.bgWhite}
                            ${styles.br100}
                            ${styles.relative}
                            ${styles.w50Fixed}
                            ${styles.h50Fixed}
                            ${styles.db}
                            ${styles.iwCenter}
                        `}>
                            <div className={`
                                ${styles.absolute}
                                ${styles.ba}
                                ${styles.br100}
                                ${styles.bGreen}
                                ${styles.bw1}
                                ${styles. w50Fixed}
                                ${styles.h50Fixed}
                                ${styles.zForeground}
                            `}></div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 32 32"
                                className={`
                                ${styles.absolute}
                                ${styles.db}
                                ${styles.darkGrey}
                                ${styles.iwCenter}
                                ${styles.zBehind}
                                ${styles.mtFixed10}
                                ${styles.mlFixed10}`}>
                            <g fill="currentColor" fillRule="evenodd" className="">
                            <path d="M9.942 16.113c3.667 3.555 8.612 4.06 12.116 0C28.141 18.966 30.813 30.93 28.977 31H3.023c-1.836-.07.844-12.04 6.919-14.887"></path><path d="M24.089 9.123a8.122 8.122 0 1 1-16.245 0 8.122 8.122 0 0 1 16.245 0"></path></g></svg>
                        </div>
                    </a>
                    <p className={`${styles.ma0} ${styles.tc}`}>
                    <a href="#" className={`${styles.f7} ${styles.fw6}`}>Sign In</a></p>
                </div>
            </div>
            <div className={`${styles.flex} ${styles.iwFlex1}`}>
                <div className={`${styles.iwCenter}`}>
                    <a href="#">
                        <div className={`
                            ${styles.bgWhite}
                            ${styles.br100}
                            ${styles.relative}
                            ${styles.w50Fixed}
                            ${styles.h50Fixed}
                            ${styles.db}
                            ${styles.iwCenter}
                        `}>
                            <div className={`
                                ${styles.absolute}
                                ${styles.ba}
                                ${styles.br100}
                                ${styles.bGreen}
                                ${styles.bw1}
                                ${styles.w50Fixed}
                                ${styles.h50Fixed}
                                ${styles.zForeground}
                            `}></div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"
                            className={`
                                ${styles.absolute}
                                ${styles.db}
                                ${styles.darkGrey}
                                ${styles.iwCenter}
                                ${styles.zBehind}
                                ${styles.mtFixed10}
                                ${styles.mlFixed10}
                            `}>
                            <path d="M14.733 10.004c0 .897-.817 1.856-1.876 1.856v15.13c0 2.147-2.927 2.147-2.927 0V11.86C8.9 11.86 8 11.066 8 9.876V1.551c0-.725 1.047-.753 1.047.028v6.154h.876v-6.21c0-.666 1.012-.71 1.012.028v6.182h.903V1.53c0-.696.98-.724.98.028v6.175h.892V1.53c0-.689 1.023-.716 1.023.028v8.446m7.808-6.518v23.496c0 2.1-2.935 2.07-2.935 0V17.66h-1.56V3.486c0-3.3 4.495-3.3 4.495 0" fill="currentColor" fillRule="evenodd"/></svg>
                        </div>
                    </a>
                    <p className={`${styles.ma0} ${styles.tc}`}>
                    <a href="#" className={`${styles.f7} ${styles.fw6}`}>Dine Now</a></p>
                </div>
            </div>
            <div className={`${styles.flex} ${styles.iwFlex1}`}>
                <div className={`${styles.iwCenter}`}>
                    <a href="#">
                        <div className={`
                            ${styles.bgWhite}
                            ${styles.br100}
                            ${styles.relative}
                            ${styles.w50Fixed}
                            ${styles.h50Fixed}
                            ${styles.db}
                            ${styles.iwCenter}
                        `}>
                            <div className={`
                                ${styles.absolute}
                                ${styles.ba}
                                ${styles.br100}
                                ${styles.bGreen}
                                ${styles.bw1}
                                ${styles.w50Fixed}
                                ${styles.h50Fixed}
                                ${styles.zForeground}
                            `}>
                            </div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="35" viewBox="0 0 60 90"
                                className={`
                                ${styles.absolute}
                                ${styles.db}
                                ${styles.darkGrey}
                                ${styles.iwCenter}
                                ${styles.zBehind}
                                ${styles.mtFixed10}
                                ${styles.mlFixed15}
                            `}>
                            <path d="M31.3 31.4C11.9 31.4 0 31.2.5 24c.3-4.6 5.8-4.4 5.8-4.4s.2-3.4 4.7-1.7c2.1-4.8 6.8-5.2 10.4-2.6 3.5-3.1 8.3-3.9 13.7 3.6 9.3-5.1 13.5 1 15.8 4 2.6-2 8.7-2.5 8.7 2 0 5.7-8.9 6.5-28.3 6.5zm20.1-10.2c-2.4-2.8-4-4.8-7.8-5.3 4.8-3 11.3-3.1 13.5 4.3-.1 0-4.3-.8-5.7 1zm-7.6-6.9c-1.3.8-1.9 1-1.9 1s-3.5-9.1-12.1-7.5c0 0 2.2-3 7.8-2.5 5.4.9 5.8 4.7 5.8 4.7s3.4-5.1 8.3-2.6c5.6 2.5 3.7 7.3 3.7 7.3s-4.7-4.2-11.6-.4zm0-6.5c-.5-1.9-4.6-3.5-4.6-3.5 3.7-2.6 8.6-2.2 10.7 1.6-3.8-.3-4.5.8-6.1 1.9zm-15.7-.5s-3.6.6-3.7 1.5C21.7 6.6 18 6.6 18 6.6S18.5 0 24.2 0s8.6 3.4 8.5 4.5c-4 1.1-4.6 2.8-4.6 2.8zm9.3 4.2c3.1 3.4 2.8 3.9 2.8 3.9s-2.5.3-4.5 1.6c-3.6-4.6-9-7.5-14.2-3.5-5.3-2.5-8.8-.8-11 2.7-2.7-.9-4.1 1.1-4.1 1.1s1-8.5 7.8-9.5 10 2.6 10 2.6 6.7-4.9 13.2 1.1zM10.8 32.6s8.2 54.9 6.5 54.9c-1.7 0-3.3-1.9-3.3-1.9L.3 29.3c4.3 3.6 10.5 3.3 10.5 3.3zm12.4.9S25 80 25 89.3c-2.3.2-5-1-5-1L15 33l8.2.5zm10.6-.4s-1.1 51.7-1.1 56.6c-2 .5-4.5 0-4.5 0s-2.8-56.6-1.7-56.6c1.1.1 7.3 0 7.3 0zm12.3-.5s-5.2 54.5-5.4 55.3c-.2.8-4.9 1.3-4.9 1.3L37.3 33l8.8-.4zm13.6-3C58.2 35.7 47.5 83.5 47 84.9c-.5 1.4-3.7 2.2-3.7 2.2s6.7-52 6.8-54.4c1.5-.1 7.1-.4 9.6-3.1z"/></svg>
                        </div>
                    </a>
                    <p className={`${styles.ma0} ${styles.tc}`}>
                    <a href="#" className={`${styles.f7} ${styles.fw6}`}>Cinema</a></p>
                </div>
            </div>
        </div>

        <h2 className={`${styles.f5} ${styles.fw4} ${styles.lightGrey} ${styles.mh3} ${styles.pa0} ${styles.lhTitle}`}>Quick Options</h2>

        <ul className={`${styles.list} ${styles.mh3} ${styles.pv3} ${styles.ph0} ${styles.mv0} ${styles.cf}`}>
            <li className={`${styles.mb3} ${styles.lhTitle} ${styles.fl} ${styles.w50} ${styles.h2}`}><a href="#" className={`${styles.white} ${styles.f6} `}><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 32 32" className={`${styles.fl} ${styles.dib} ${styles.mr2}`}><path fill="currentColor" fillRule="evenodd" d="M29.102 21.332a2.157 2.157 0 0 1-2.156 2.158H5.056a2.157 2.157 0 0 1-2.159-2.156V17.13h26.205v4.202zM27.884 6H4.116A3.116 3.116 0 0 0 1 9.116V22.13a3.116 3.116 0 0 0 3.118 3.116h23.766A3.116 3.116 0 0 0 31 22.127V9.117A3.116 3.116 0 0 0 27.884 6z"/></svg>Buy tastecard</a></li>
            <li className={`${styles.mb3} ${styles.lhTitle} ${styles.fl} ${styles.w50} ${styles.h2}`}><a href="#" className={`${styles.white} ${styles.f6} `}><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 32 32" className={`${styles.fl} ${styles.dib} ${styles.mr2}`}><path fill="currentColor" fillRule="evenodd" d="M22.848 25.825l-3.818-3.817 1.888-1.887 1.93 1.909 4.681-4.704 1.91 1.912-6.591 6.587zm1.387-11.016a6.768 6.768 0 0 0-6.767 6.77 6.764 6.764 0 0 0 6.767 6.762A6.764 6.764 0 0 0 31 21.578a6.767 6.767 0 0 0-6.765-6.769zM2.879 8.36h23.055V5.88H2.879v2.48zM1 5.88v17.5c0 1.038.84 1.878 1.879 1.878h13.157v-1.879H2.879V13.171h24.933V5.879c0-1.039-.84-1.879-1.878-1.879H2.879C1.84 4 1 4.84 1 5.879z"></path></svg>Redeem code</a></li>
            <li className={`${styles.mb3} ${styles.lhTitle} ${styles.fl} ${styles.w50} ${styles.h2}`}><a href="#" className={`${styles.white} ${styles.f6} `}><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 32 32" className={`${styles.fl} ${styles.dib} ${styles.mr2}`}><g fill="currentColor" fillRule="evenodd"><path d="M2.879 8.314h23.055v-2.48H2.879v2.48zm0 4.811h24.933V5.833c0-1.039-.84-1.879-1.878-1.879H2.879C1.84 3.954 1 4.794 1 5.834v17.498c0 1.04.84 1.88 1.879 1.88h13.159v-1.88H2.878V13.125z"></path><path d="M28.709 22.003c-.004.388-.206.593-.593.595-.787.003-1.575.007-2.36-.004-.18-.002-.218.054-.216.223.01.76.004 1.519.004 2.278 0 .48-.188.67-.657.671-.294 0-.588.002-.883 0-.405-.003-.605-.204-.607-.611-.002-.769-.01-1.538.006-2.306.005-.22-.068-.257-.265-.255-.726.013-1.45-.006-2.173.01-.31.007-.55-.083-.733-.327v-1.502c.109-.334.384-.396.682-.4.672-.01 1.341-.001 2.012-.003.385-.002.387-.004.388-.39.002-.671-.005-1.34.004-2.012.004-.3.068-.572.401-.684h1.45c.287.151.386.391.38.714-.015.75 0 1.502-.009 2.252-.002.165.04.208.204.206.77-.01 1.54-.006 2.307-.004.472 0 .658.188.658.664 0 .294.003.59 0 .885zm-4.474-7.24a6.768 6.768 0 0 0-6.767 6.77 6.764 6.764 0 0 0 6.767 6.763A6.764 6.764 0 0 0 31 21.532a6.767 6.767 0 0 0-6.765-6.768z"></path></g></svg>tastecard +</a></li>
            <li className={`${styles.mb3} ${styles.lhTitle} ${styles.fl} ${styles.w50} ${styles.h2}`}><a href="#" className={`${styles.white} ${styles.f6} `}><svg xmlns="http://www.w3.org/2000/svg" width="20" height="21" viewBox="0 0 32 31" className={`${styles.fl} ${styles.dib} ${styles.mr2}`}><path fill="none" stroke="currentColor" d="M5.03 17.76c.288.314.604.6.942.858l.257.171.236.129.193.107.214.107h4.756l.214-.107.193-.107.236-.129.257-.171a7.241 7.241 0 0 0 1.82-.857 11.14 11.14 0 0 1 4.028 8.848c.001.827-.078 1.652-.235 2.464-2.785.564-5.62.844-8.462.835a41.904 41.904 0 0 1-8.441-.835 12.854 12.854 0 0 1-.236-2.443 11.14 11.14 0 0 1 4.028-8.87zM9.679 7.393a4.649 4.649 0 0 1 3.599 7.605l-.129.15-.3.322-.343.278-.407.257a4.563 4.563 0 0 1-1.82.6h-.965a4.563 4.563 0 0 1-1.885-.557l-.407-.257-.343-.279-.3-.32-.128-.15a4.649 4.649 0 0 1 3.428-7.649zm7.305 9.126c.442-.876.67-1.846.664-2.828a5.913 5.913 0 0 0-.214-1.65l.278-.235a6.256 6.256 0 0 0 9.32 0 11.119 11.119 0 0 1 3.963 8.784c-.001.812-.08 1.623-.236 2.42a41.904 41.904 0 0 1-8.44.836h-.965a12.233 12.233 0 0 0-4.37-7.327zm5.442-5.891a4.649 4.649 0 1 1-.258-9.294 4.649 4.649 0 0 1 .258 9.294z"/></svg>Gift tastecard</a></li>
            <li className={`${styles.mb3} ${styles.lhTitle} ${styles.fl} ${styles.w50} ${styles.h2}`}><a href="#" className={`${styles.white} ${styles.f6} `}><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 32 32" className={`${styles.fl} ${styles.dib} ${styles.mr2}`}><path fill="currentColor" fillRule="evenodd" d="M9.539 24.989H22.57V3.559H9.539v21.43zm6.517 4.657a1.652 1.652 0 1 1 0-3.305 1.654 1.654 0 0 1 0 3.306zM22.532 1H9.58C8.16 1 7 2.16 7 3.578v24.844A2.587 2.587 0 0 0 9.58 31h12.952a2.586 2.586 0 0 0 2.579-2.578V3.578A2.586 2.586 0 0 0 22.532 1z"/></svg>Contact us</a></li>
            <li className={`${styles.mb3} ${styles.lhTitle} ${styles.fl} ${styles.w50} ${styles.h2}`}><a href="#" className={`${styles.white} ${styles.f6} `}><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 32 33" className={`${styles.fl} ${styles.dib} ${styles.mr2}`}><path d="M32 19.2v-6.4h-3.567a12.53 12.53 0 0 0-1.341-3.375l2.483-2.475-4.525-4.525-2.425 2.425A12.596 12.596 0 0 0 19.2 3.408V0h-6.4v3.408A12.861 12.861 0 0 0 9.375 4.85L6.95 2.425 2.425 6.95l2.483 2.475A12.808 12.808 0 0 0 3.567 12.8H0v6.4h3.675a13.005 13.005 0 0 0 1.392 3.217l-2.642 2.641 4.525 4.525 2.692-2.691c.975.566 2.041.991 3.158 1.283v3.833h6.4v-3.833a12.505 12.505 0 0 0 3.158-1.283l2.692 2.691 4.525-4.525-2.633-2.641a12.74 12.74 0 0 0 1.391-3.217H32zm-16 1.283A4.478 4.478 0 0 1 11.525 16c0-2.475 2-4.483 4.475-4.483A4.485 4.485 0 0 1 20.483 16 4.485 4.485 0 0 1 16 20.483z"/></svg>Settings</a></li>
        </ul>
    </nav>
);

export default (
    <Header
        promoButtons={buttons}
        icon={icon}
        navContent={navContent}
        searchOnSubmit={onSubmit}
        searchOnChange={valueDidChange}
    />
 );
