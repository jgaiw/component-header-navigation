## Introduction 

This component will display the header for a page. The icon passed in is typically an SVG icon. This will appear on both regular and compact screens. There is a search on change call back executes once the user has typed something into the search bar. The searchOnSubmit call back is executed when the user clicks the search button. When the browser enters compact width the layout changes. Clicking the menu icon will then display the Navigation element which is passed in as a prop

run: ./samples/introduction

```
<Header
    promoButtons={buttons}
    icon={icon}
    navContent={navContent}
    searchOnSubmit={onSubmit}
    searchOnChange={valueDidChange}
/>

const buttons: JSX.Element = (
    <div>
        <div className="dib"}>
            <Button onClick={doAlert} style="navigation">Sign In</Button>
        </div>
        <div className="dib pl2">
            <Button onClick={doAlert} style="navigation">Redeem</Button>
        </div>
        <div className="dib pl2">
            <Button onClick={doAlert}> Buy</Button>
        </div>
    </div>
);

const doAlert = () =>  alert('Clicked a Button');
const onSubmit = (value: string) => alert(value);
const valueDidChange = (value: string) => console.log(value);
const icon = <svg/> // tasecard svg too long for example code
```
