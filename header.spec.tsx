import { expect } from 'chai';
import * as styles from 'designs/css/application.min.css';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';
import Header from './header';

describe('<Header />', () => {

    let simpleWrapper: ShallowWrapper<any, any>;
    const icon = <div id="icon">Test Icon</div>;
    const promoButtons = <div id="buttons">Buttons</div>;
    const navContent = <div id="navigate">Navigate</div>;
    const callBack = sinon.stub();
    const searchSubmit = sinon.stub();
    const compactSearchLink = 'search page url';

    beforeEach( () => {
        simpleWrapper = shallow(<Header
            icon={icon}
            promoButtons={promoButtons}
            navContent={navContent}
            searchOnChange={callBack}
            searchOnSubmit={searchSubmit}
            compactSearchLink={compactSearchLink}
        />);
    });

    it('should content passed in icon', () => {
        // Should find two instances of icon one for regular width
        // and the other for the compact width
        expect(simpleWrapper.find('#icon').length).to.equal(2);
        expect(simpleWrapper.find('#buttons').length).to.equal(1);
        expect(simpleWrapper.find('#navigate').length).to.equal(1);
    });

    it('can detect change in value of the search bar', () => {
        // Given
        const inputs = simpleWrapper.find('input');
        const searchBar = inputs.find('.' + styles.f5);

        // When
        searchBar.simulate('keydown', { which: 'a' });

        // Then
        callBack.withArgs(sinon.match.string).returns('a');
    });

    it('should execute a call back with search value when form is submitted', () => {
        // Given
        const inputs = simpleWrapper.find('input');
        const searchBar = inputs.find('.' + styles.f5);
        searchBar.simulate('keydown', { which: 'a' });

        // When
        const submitButton = simpleWrapper.find('button');
        submitButton.simulate('click');

        // Then
        searchSubmit.withArgs(sinon.match.string).returns('a');
    });

    it('directs you to the search page if the compact width search link is clicked', () => {
        const searchLink = simpleWrapper.find('a');
        expect(searchLink.prop('href')).to.equal('search page url');
    });

});
