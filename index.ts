import HeaderNavigation from './header';

const examples = {
    Introduction: require('./examples/introduction.md').default,
};

export {HeaderNavigation, examples};
