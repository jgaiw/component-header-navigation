import * as styles from 'designs/css/application.min.css';
import * as React from 'react';

interface IProps {
    icon: JSX.Element;
    searchOnChange?: (value: string) => void;
    searchOnSubmit?: (value: string) => void;
    compactSearchLink?: string;
    promoButtons: JSX.Element;
    navContent: JSX.Element;
}

interface IState {
    searchValue: string;
}

export default class Header extends React.Component<IProps, IState> {

    public constructor(props: IProps) {
        super(props);
        this.state = {
            searchValue: '',
        };
    }

    public render(): JSX.Element {
        const headerStylesClasses = [
            styles.fixed,
            styles.left0,
            styles.bgBlack,
            styles.w100,
            styles.h3,
            styles.zForeground,
            styles.top0,
        ].join(' ');
        const regularWidthClasses = [
            styles.relative,
            styles.dn,
            styles.flexL,
            styles.w100,
            styles.mwSite,
            styles.iwCenter,
        ].join(' ');
        const compactWidthClasses = [
            styles.dnL,
            styles.flex,
            styles.itemsCenter,
        ].join(' ');
        return (
            <div>
                <input type="checkbox" id={`${styles.showMenu}`} className={`${styles.absolute} ${styles.o0}`}/>
                <label htmlFor={`${styles.showMenu}`}></label>
                <header className={headerStylesClasses}>
                    <div className={regularWidthClasses}>
                        {this.getIcon(this.props.icon)}
                        {this.getSearchBar()}
                        {this.getPromoButtons(this.props.promoButtons)}
                    </div>
                    <div className={compactWidthClasses}>
                        {this.getMenuIcon()}
                        {this.getIcon(this.props.icon, true)}
                        {this.getCompactSearch()}
                    </div>
                </header>
                    {this.props.navContent}
            </div>
        );
    }

    private getIcon(icon: JSX.Element, isCompact: boolean = false ): JSX.Element {
        const classes = isCompact
            ? [
                styles.flex,
                styles.iwFlex1,
                styles.pv3,
                styles.ph2,
            ]
            : [
                styles.flex,
                styles.pt3Ns,
                styles.pl3Ns,
                styles.iwFlexItem,
                styles.iwFlex1,
                styles.mwFixed200,
            ];
        return (
            <div className={classes.join(' ')}>
                {icon}
            </div>
        );
    }

    private getCompactSearch(): JSX.Element {
        const classes = [
            styles.flex,
            styles.iwFlex1,
            styles.itemsCenter,
            styles.relative,
            styles.pv3,
            styles.ph2,
            styles.tr,
        ].join(' ');
        const svgClasses = [
            styles.dib,
            styles.white,
            styles.mr1,
        ].join(' ');
        /* tslint:disable:max-line-length */
        return (
            <div className={classes}>
                <a href={this.props.compactSearchLink} title="Search For Restaurants" className={`${styles.white} ${styles.f6} ${styles.w100}`}>
                <svg xmlns="http://www.w3.org/2000/svg"
                    width="15"
                    height="15"
                    viewBox="0 0 32 32"
                    className={svgClasses}
                >
                    <path fill="currentColor" fillRule="evenodd" d="M12.25 19.75a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm18.166 7.839l-8.924-8.927A11.203 11.203 0 0 0 23.5 12.25C23.5 6.038 18.462 1 12.25 1S1 6.038 1 12.25C1 18.463 6.038 23.5 12.25 23.5a11.19 11.19 0 0 0 6.412-2.008l8.927 8.924c.776.779 2.05.779 2.827 0a2.004 2.004 0 0 0 0-2.827z"/>
                </svg>
                <span className={`${styles.dib} ${styles.fr} ${styles.mtNegative1}`}>Search</span></a>
            </div>
        );
        /* tslint:enable:max-line-length */
    }

    private getSearchBar(): JSX.Element {
        const rootClasses = [
            styles.flex,
            styles.pt3Ns,
            styles.pl3Ns,
            styles.iwFlexItem,
            styles.iwFlex1,
        ].join(' ');
        const searchClasses = [
            styles.flex,
            styles.w100,
            styles.br2,
            styles.ba,
            styles.bLightGrey,
            styles.overflowHidden,
        ].join(' ');
        const inputClasses = [
            styles.iwFlex1,
            styles.pa2,
            styles.ma0,
            styles.f5,
            styles.bn,
        ].join(' ');
        const buttonClasses = [
            styles.pa2,
            styles.ma0,
            styles.bgWhite,
            styles.bn,
            styles.cursorPointer,
        ].join(' ');
        /* tslint:disable:max-line-length */
        return (
            <form onSubmit={(event) => this.searchSubmit(event)} className={rootClasses}>
                <div className={searchClasses}>
                    <input type="text" className={inputClasses}
                        placeholder="Search for Restaurants"
                        onChange={(event) => this.searchOnChange(event)}
                        value={this.state.searchValue}
                    />
                    <button className={buttonClasses}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 32 32" className={`${styles.darkGrey}`}>
                        <path fill="currentColor" fillRule="evenodd" d="M12.25 19.75a7.5 7.5 0 1 1 0-15 7.5 7.5 0 0 1 0 15zm18.166 7.839l-8.924-8.927A11.203 11.203 0 0 0 23.5 12.25C23.5 6.038 18.462 1 12.25 1S1 6.038 1 12.25C1 18.463 6.038 23.5 12.25 23.5a11.19 11.19 0 0 0 6.412-2.008l8.927 8.924c.776.779 2.05.779 2.827 0a2.004 2.004 0 0 0 0-2.827z"/></svg>
                    </button>
                </div>
            </form>
        );
        /* tslint:enable:max-line-length */
    }

    private searchOnChange(event: React.ChangeEvent<HTMLInputElement>) {
        const value = event.target.value;
        this.setState( { searchValue: value } );
        if (this.props.searchOnChange) {
            this.props.searchOnChange(value);
        }
    }

    private searchSubmit(event: React.FormEvent<HTMLFormElement>) {
        if (this.props.searchOnSubmit) {
            this.props.searchOnSubmit(this.state.searchValue);
        }
        event.preventDefault();
    }

    private getMenuIcon(): JSX.Element {
        return (
            <div className={`${styles.flex} ${styles.iwFlex1} ${styles.pv3} ${styles.ph2}`}>
                <div className={`${styles.w100} ${styles.db}`}>
                    <label className={`
                        ${styles.fl}
                        ${styles.dib}
                        ${styles.relative}
                        ${styles.w16Fixed}
                        ${styles.h15Fixed}
                        ${styles.cursorPointer}
                        ${styles.white}`}
                        htmlFor={`${styles.showMenu}`}
                        id={`${styles.menuIcon}`}
                    >
                       {this.getMenuSpan()}
                       {this.getMenuSpan()}
                       {this.getMenuSpan()}
                       {this.getMenuSpan()}
                    </label>
                    <label className={`${styles.fl} ${styles.dib}`} htmlFor={`${styles.showMenu}`}>
                        <span className={`${styles.dib}
                            ${styles.fl}
                            ${styles.mtNegative2}
                            ${styles.ml2}
                            ${styles.white}
                            ${styles.cursorPointer}
                            ${styles.hoverGreen}
                            ${styles.f6}`}
                        >
                            Menu
                        </span>
                    </label>
                </div>
            </div>
        );
    }

    private getPromoButtons(buttons: JSX.Element | JSX.Element[]): JSX.Element {
        const rootClasses = [
            styles.flex,
            styles.pt3Ns,
            styles.iwFlexItem,
            styles.iwFlex1,
            styles.itemsCenter,
        ].join(' ');
        const positionClasses = [
            styles.absolute,
            styles.mr3,
            styles.right0,
            styles.dib,
        ].join(' ');
        return (
            <div className={rootClasses}>
                <div className={positionClasses}>
                    {buttons}
                </div>
            </div>
        );
    }

    private getMenuSpan(): JSX.Element {
        return (
            <span className={`
                ${styles.absolute}
                ${styles.db}
                ${styles.bgWhite}
                ${styles.o100}
                ${styles.w16Fixed}
                ${styles.h1Fixed}
            `}></span>
        );
    }
}
